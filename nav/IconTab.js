
import React from 'react';
import { createBottomTabNavigator, createAppContainer,createStackNavigator } from 'react-navigation';

import Dashboard from "../page/Dashboard";
import MainLayanan from "../page/layanan/MainLayanan";

export default class IconTab extends React.Component {
     constructor(props) {
         super(props);
     }

     render() {
         var url = this.props.url;
         return (
             <Image source = { require(url) } style = { { width: 20, height: 20 } } />
         );
     }
}