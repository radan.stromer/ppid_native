import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import {
  Container,
  Content,
  Text,
  Button,
  H3,
  List,
  ListItem
} from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { withNavigation } from 'react-navigation';
import { Constants } from 'expo';
import LogoHeader from '../component/LogoHeader'; 



export default class Regulasi extends React.Component {
  static navigationOptions = {
    headerTitle: <LogoHeader />,
    headerStyle: {
      backgroundColor:'green',
      paddingBottom:20,
      height:88
    },
  };
  
  render() {
    return (
      <Container style={{padding:10}}>
        <ScrollView>
		        <List>
					<ListItem itemDivider>
						<Text style={{fontWeight:'bold', fontSize:13}}>Peraturan perundang-undangan yang menjadi dasar layanan PPID BP2MI</Text>
					</ListItem>
					
					<ListItem style={{paddingBottom: 1,}}>
					  <Text style={{width:'70%', fontSize:14}}>A. UU Nomor.14 Tahun 2008</Text>
					  <View style={{
					    flex: 1,
					    flexDirection: 'row',
					    justifyContent: 'flex-end',
					  }}>
					    <TouchableOpacity onPress={() => this.props.navigation.navigate('Regulasi1')} style={{backgroundColor:'green',borderRadius:2, paddingBottom:2,paddingTop:2,height:20,width:70}}>
					      <Text style={{fontSize:12,textAlign:'center',color:'white'}}>
					        View
					      </Text>
					    </TouchableOpacity>
					  </View>
					</ListItem>

					<ListItem style={{paddingBottom: 1,}}>
						<Text style={{width:'70%', fontSize:14}}>B. UU Nomor.18 tahun 2017</Text>
						<View style={{
							flex: 1,
							flexDirection: 'row',
							justifyContent: 'flex-end',
						}}>
							<TouchableOpacity onPress={() => this.props.navigation.navigate('Regulasi2')} style={{backgroundColor:'green',borderRadius:2, paddingBottom:2,paddingTop:2,height:20,width:70}}>
								<Text style={{fontSize:12,textAlign:'center',color:'white'}}>
									View
								</Text>
							</TouchableOpacity>
						</View>
					</ListItem>

					<ListItem style={{paddingBottom: 1,}}>
						<Text style={{width:'70%', fontSize:14}}>
							C. Perka BNP2TKI NOMOR : PER. 13/KA/II/2014
						</Text>
						<View style={{
							flex: 1,
							flexDirection: 'row',
							justifyContent: 'flex-end',
						}}>
							<TouchableOpacity onPress={() => this.props.navigation.navigate('Regulasi3')} style={{backgroundColor:'green',borderRadius:2, paddingBottom:2,paddingTop:2,height:20,width:70}}>
								<Text style={{fontSize:12,textAlign:'center',color:'white'}}>
									View
								</Text>
							</TouchableOpacity>
						</View>
					</ListItem>
		    </List>
	        </ScrollView>
      </Container>
    );
  }
}