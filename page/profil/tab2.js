import React, { Component } from 'react';
import { StyleSheet, View,Image,ImageBackground,Dimensions,TouchableOpacity } from "react-native";
import { Container, Header, Content, Card, CardItem, Text, Body,H3 } from 'native-base';
export default class Tab2 extends React.Component {
  render() {
    return (
      <Container>
        <Content>
          <Card>
            <CardItem header style={{backgroundColor:'#16529b',paddingBottom:5,paddingTop:5}}>
              <Text style={{color:'white', fontSize:14}}>PPID BP2MI</Text>
            </CardItem>
            <CardItem>
              <Body>
                <Text style={{textAlign:'justify'}}>
                  Biro Hukum dan Humas BP2MI Lantai I, Jl. MT Haryono Kav.52 Pancoran, Jakarta Selatan 12095
                </Text>
              </Body>
            </CardItem>
         </Card>

         <Card>
            <CardItem header style={{backgroundColor:'#16529b',paddingBottom:5,paddingTop:5}}>
              <Text style={{color:'white', fontSize:14}}>PPID Perwakilan</Text>
            </CardItem>
            <CardItem>
              <Body>
                <Text>BP3TKI Aceh</Text>
                <Text style={{textAlign:'justify'}}>
                  Jln. Soekarno Hatta No. 17, Banda Aceh, NAD, Tlp. 0651- 7410355, Fax. 0651- 49186
                </Text>
              </Body>
            </CardItem>
            <View
              style={{
                borderBottomColor: 'black',
                borderBottomWidth: 1,
              }}
            />
            <CardItem>
              <Body>
                <Text>BP3TKI Medan</Text>
                <Text>
                  Jln. Pendidikan No.357, Marindal, Medan, Sumatera Utara, Tlp./Faks. 061-7869304
                </Text>
              </Body>
            </CardItem>
            <View
              style={{
                borderBottomColor: 'black',
                borderBottomWidth: 1,
              }}
            />
            <CardItem>
              <Body>
                <Text>BP3TKI Tanjung Pinang</Text>
                <Text>
                  Jln. Nusantara KM 13 Arah Kijang, Tanjung Pinang, Kep. Riau, Tlp. 0771-7004553 , Fax. 0771-8038621
                </Text>
              </Body>
            </CardItem>
            <View
              style={{
                borderBottomColor: 'black',
                borderBottomWidth: 1,
              }}
            />
            <CardItem>
              <Body>
                <Text>BP3TKI Pekanbaru</Text>
                <Text>
                  Jln. Taman Sari Gg. Taman Sari I Kel. Tangkerang Selatan, Pekanbaru, 28288, Riau, Tlp./Fax 0761- 38894
                </Text>
              </Body>
            </CardItem>
            <View
              style={{
                borderBottomColor: 'black',
                borderBottomWidth: 1,
              }}
            />
            <CardItem>
              <Body>
                <Text>BP3TKI Padang</Text>
                <Text>
                  Jl. Rasuna Said No.91,Padang, Sumatera Barat.  Tlp.0751- 444757, Fax. 0751-444757
                </Text>
              </Body>
            </CardItem>
            <View
              style={{
                borderBottomColor: 'black',
                borderBottomWidth: 1,
              }}
            />
            <CardItem>
              <Body>
                <Text>BP3TKI Palembang</Text>
                <Text>
                  Jl. Dwikora II No.1220 Palembang 30137, Sumatera Selatan, Tlp. 0711-312062, Fax. 0711-365606
                </Text>
              </Body>
            </CardItem>
            <View
              style={{
                borderBottomColor: 'black',
                borderBottomWidth: 1,
              }}
            />
            <CardItem>
              <Body>
                <Text>BP3TKI Bandar Lampung</Text>
                <Text>
                  Jl. Untung Suropati No.21A, Labuhan Ratu, Bandar Lampung 35142, Tlp. 0721-774385
                </Text>
              </Body>
            </CardItem>
            <View
              style={{
                borderBottomColor: 'black',
                borderBottomWidth: 1,
              }}
            />
            <CardItem>
              <Body>
                <Text>BP3TKI Jakarta</Text>
                <Text>
                  Jl. Pengantin Ali I No. 71, Ciracas, Jakarta Timur, DKI Jakarta, Tlp. 021- 87781840, Fax. 021- 87781841
                </Text>
              </Body>
            </CardItem>
            <View
              style={{
                borderBottomColor: 'black',
                borderBottomWidth: 1,
              }}
            />
            <CardItem>
              <Body>
                <Text>BP3TKI Serang</Text>
                <Text>
                  Jln. Ciwaru Raya Komp. Depag No. 2, Serang, Banten, Tlp. 0254-204970, Fax. 0254-207963
                </Text>
              </Body>
            </CardItem>
            <View
              style={{
                borderBottomColor: 'black',
                borderBottomWidth: 1,
              }}
            />
            <CardItem>
              <Body>
                <Text>BP3TKI Bandung</Text>
                <Text>
                  Jl. Soekarno Hatta No. 587 Kiara Condong, Bandung, Jawa Barat, 40234, Tlp/Fax. 022-7336965
                </Text>
              </Body>
            </CardItem>
            <View
              style={{
                borderBottomColor: 'black',
                borderBottomWidth: 1,
              }}
            />
            <CardItem>
              <Body>
                <Text>BP3TKI Yogyakarta</Text>
                <Text>
                  Jl. Sambisari No. 311A Juwangen, Purwomatani, Kalasan,  Sleman,  Yogyakarta, Tlp:0274-497403,  Fax. 0274-497442
                </Text>
              </Body>
            </CardItem>
            <View
              style={{
                borderBottomColor: 'black',
                borderBottomWidth: 1,
              }}
            />
            <CardItem>
              <Body>
                <Text>BP3TKI Semarang</Text>
                <Text>
                  Jl. Kalipepe III/64 Pundak Payung, Semarang, Jawa Tengah, 50236, Tlp. 024-70799273, Fax. 024-7477223
                </Text>
              </Body>
            </CardItem>
            <View
              style={{
                borderBottomColor: 'black',
                borderBottomWidth: 1,
              }}
            />
            <CardItem>
              <Body>
                <Text>BP3TKI Denpasar</Text>
                <Text>
                  Jl. Danau Tempe No 29, Denpasar, Bali, Telp. (0361) 4721057, Faks. (0361) 4721099
                </Text>
              </Body>
            </CardItem>
            <View
              style={{
                borderBottomColor: 'black',
                borderBottomWidth: 1,
              }}
            />
            <CardItem>
              <Body>
                <Text>BP3TKI Mataram</Text>
                <Text>
                  Jl Adi Sucipto No. 9 Mataram, Nusa Tenggara Barat, Telp: 0370-633797, Fax:  0370 639712
                </Text>
              </Body>
            </CardItem>
            <View
              style={{
                borderBottomColor: 'black',
                borderBottomWidth: 1,
              }}
            />
            <CardItem>
              <Body>
                <Text>BP3TKI Kupang</Text>
                <Text>
                  Jl. Perintis Kemerdekaan I No. 6 Kel. Oebufu Kupang, Nusa Tenggara Timur,Tlp. 0380-825355, Fax. 0380-839653
                </Text>
              </Body>
            </CardItem>
            <View
              style={{
                borderBottomColor: 'black',
                borderBottomWidth: 1,
              }}
            />
            <CardItem>
              <Body>
                <Text>BP3TKI Banjarbaru</Text>
                <Text>
                  Jln. Rosela I No. 17 Simpang 4 Sei Ulin, Banjar Baru, Kalimantan Selatan, Telp. 0511- 4781638, Fax. 0511-4782352
                </Text>
              </Body>
            </CardItem>
            <View
              style={{
                borderBottomColor: 'black',
                borderBottomWidth: 1,
              }}
            />
            <CardItem>
              <Body>
                <Text>BP3TKI Pontianak</Text>
                <Text>
                  Jl. Urai Bawadi No. 82 B Pontianak, Kalimantan Barat, Tlp. 0561-741564, Fax. 0561- 760566
                </Text>
              </Body>
            </CardItem>
            <View
              style={{
                borderBottomColor: 'black',
                borderBottomWidth: 1,
              }}
            />
            <CardItem>
              <Body>
                <Text>BP3TKI Nunukan</Text>
                <Text>
                  Jl. Raya Tien Soeharto No. 12 Nunukan, Kalimantan Utara, Tlp. 0556-21018,Fax. 0556-21018
                </Text>
              </Body>
            </CardItem>
            <View
              style={{
                borderBottomColor: 'black',
                borderBottomWidth: 1,
              }}
            />
            <CardItem>
              <Body>
                <Text>BP3TKI Manado</Text>
                <Text>
                  Jl. Babe Palar No. 96 Manado, Sulawesi Utara, 95115, Tlp/Fax : 0431850696
                </Text>
              </Body>
            </CardItem>
            <View
              style={{
                borderBottomColor: 'black',
                borderBottomWidth: 1,
              }}
            />

            <CardItem>
              <Body>
                <Text>BP3TKI Makassar</Text>
                <Text>
                  Jl. Pacinang Raya No. 104 Tello Baru, Makassar,   Tlp. 0411-425038, Fax. 0411- 425039
                </Text>
              </Body>
            </CardItem>
            <View
              style={{
                borderBottomColor: 'black',
                borderBottomWidth: 1,
              }}
            />
         </Card>
         
         <Card>
            <CardItem header style={{backgroundColor:'#16529b',paddingBottom:5,paddingTop:5}}>
              <Text style={{color:'white', fontSize:14}}>LP3TKI</Text>
            </CardItem>
            <CardItem>
              <Body>
                <Text>LP3TKI Palu</Text>
                <Text>
                  Jl. Diponegoro No.2, Kel. Baru, Palu Barat, Palu, Sulawesi Tengah, Tlp./Fax. 0451-4014616
                </Text>
              </Body>
            </CardItem>
            <View
              style={{
                borderBottomColor: 'black',
                borderBottomWidth: 1,
              }}
            />
            <CardItem>
              <Body>
                <Text>LP3TKI Kendari</Text>
                <Text>
                  Jl. Kancil Lorong Ganesha No.1, Anduonohu, Kendari Sulawesi Tenggara. Tlp./Fax. 0401-3195094
                </Text>
              </Body>
            </CardItem>
            <View
              style={{
                borderBottomColor: 'black',
                borderBottomWidth: 1,
              }}
            />
            <CardItem>
              <Body>
                <Text>LP3TKI Surabaya</Text>
                <Text>
                  Jl. Jemursari No.99 Wonocolo, Surabaya, Jawa Timur, Tlp. 031-8434448
                </Text>
              </Body>
            </CardItem>
            <View
              style={{
                borderBottomColor: 'black',
                borderBottomWidth: 1,
              }}
            />
         </Card>
        </Content>
      </Container>
    );
  }
}