import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity,ScrollView } from 'react-native';
import { Container, Content, Text, Button,H3, List, ListItem } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { withNavigation } from 'react-navigation';
import LogoHeader from '../../component/LogoHeader'; 


 class Tab1 extends React.Component {

  static navigationOptions = {
    headerTitle: <LogoHeader />,
    headerStyle: {
      backgroundColor:'green',
      paddingBottom:20,
      height:88
    },
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      		<Container style={{padding:10}}>
	      	<ScrollView>
		        <List>
					<ListItem itemDivider>
						<Text style={{fontWeight:'bold', fontSize:13}}>Data Penempatan dan Perlindungan Pekerja Migran Indonesia</Text>
					</ListItem>
					
					<ListItem style={{paddingBottom: 1,}}>
					  <Text style={{width:'70%', fontSize:14}}>1. Data Penempatan dan Perlindungan PMI Januari-Februari 2019</Text>
					  <View style={{
					    flex: 1,
					    flexDirection: 'row',
					    justifyContent: 'flex-end',
					  }}>
					    <TouchableOpacity onPress={() => this.props.navigation.navigate('Berkala1')} style={{backgroundColor:'green',borderRadius:2, paddingBottom:2,paddingTop:2,height:20,width:70}}>
					      <Text style={{fontSize:12,textAlign:'center',color:'white'}}>
					        View
					      </Text>
					    </TouchableOpacity>
					  </View>
					</ListItem>

					<ListItem style={{paddingBottom: 1,}}>
						<Text style={{width:'70%', fontSize:14}}>2. Data Penempatan dan Perlindungan PMI 2018</Text>
						<View style={{
							flex: 1,
							flexDirection: 'row',
							justifyContent: 'flex-end',
						}}>
							<TouchableOpacity onPress={() => this.props.navigation.navigate('Berkala2')} style={{backgroundColor:'green',borderRadius:2, paddingBottom:2,paddingTop:2,height:20,width:70}}>
								<Text style={{fontSize:12,textAlign:'center',color:'white'}}>
									View
								</Text>
							</TouchableOpacity>
						</View>
					</ListItem>

					<ListItem style={{paddingBottom: 1,}}>
						<Text style={{width:'70%', fontSize:14}}>3. Data Penempatan dan Perlindungan PMI Maret 2019</Text>
						<View style={{
							flex: 1,
							flexDirection: 'row',
							justifyContent: 'flex-end',
						}}>
							<TouchableOpacity onPress={() => this.props.navigation.navigate('Berkala3')} style={{backgroundColor:'green',borderRadius:2, paddingBottom:2,paddingTop:2,height:20,width:70}}>
								<Text style={{fontSize:12,textAlign:'center',color:'white'}}>
									View
								</Text>
							</TouchableOpacity>
						</View>
					</ListItem>
		        </List>
	        </ScrollView>
        </Container>
    );
  }
}

export default withNavigation(Tab1);