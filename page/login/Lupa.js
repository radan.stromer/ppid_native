import React, { Component } from 'react';
import { View,Image } from "react-native";
import { Container, Header, Content, Tab, Tabs, Form, Item, Input,Button,Text, H1 } from 'native-base';
import LogoHeader2 from '../../component/LogoHeader2';
import { Col, Row, Grid } from 'react-native-easy-grid';
import * as firebase from 'firebase';
const firebaseConfig = {
    apiKey: "AIzaSyBMBa8FQQNlW3lzIoGK-qSHI1KNwZzmvj0",
    authDomain: "ppid-48a0a.firebaseapp.com",
    databaseURL: "https://ppid-48a0a.firebaseio.com",
    projectId: "ppid-48a0a",
    storageBucket: "ppid-48a0a.appspot.com"
}

try{
    firebase.initializeApp(firebaseConfig);
}catch(err){
    if (!/already exists/.test(err.message)) {
        console.error('Firebase initialization error', err.stack)
    }
}

export default class Lupa extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email:'',
            errorMessage: null,
            loading:false,
        };
    }

    static navigationOptions = {
    headerTitle: <LogoHeader2 />,
    headerStyle: {
        backgroundColor:'white',
        paddingBottom:20,
        height:88
    },
    };

    handleLupa = () => {
        this.setState({
            loading: true
        });
        var auth = firebase.auth();
        var emailAddress = this.state.email;

        auth.sendPasswordResetEmail(emailAddress).then(()=>{
            this.setState({
                errorMessage: 'Link reset password sudah terkirim. Silahkan cek email Anda.'
            });
        }).catch(function (error) {
            error => this.setState({
                errorMessage: error.message,
                loading: false
            })
        });
    }

    render() {
    return (
        <Container style={{padding:10}}>
            <Content>
                    <Text style={{fontWeight:'bold', fontSize:14,marginBottom:10, textAlign:'justify'}}>Masukan email terdaftar untuk mendapatkan link reset Password.</Text>
                    {this.state.errorMessage &&
                    <Text style={{ color: 'red' }}>
                        {this.state.errorMessage}
                    </Text>}
                    <Form>
                    <Item regular style={{marginBottom:15}}>
                        <Input onChangeText={email=>this.setState({email})} placeholder="Email" keyboardType="email-address"/>
                    </Item>
                    <Button full onPress={this.handleLupa}>
                        <Text>
                        Reset Password
                        </Text>
                    </Button>
                    {this.state.loading &&
                    <Text style={{ color: 'green' }}>
                    Loading
                    </Text>}
                    </Form>
            </Content>
        </Container>
    );
    }
}