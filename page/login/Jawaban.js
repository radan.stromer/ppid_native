import React, { Component } from 'react';
import { View,Image,KeyboardAvoidingView,Linking,TouchableOpacity } from "react-native";
import {
  Container,
  Header,
  Content,
  Tab,
  Tabs,
  Form,
  Item,
  Input,
  Button,
  Text,
  H3,
  Textarea,
  Picker,
  Icon,
  Card,
  CardItem
} from 'native-base';
import LogoHeader2 from '../../component/LogoHeader2';
import LogoHeader from '../../component/LogoHeader'; 
import { Col, Row, Grid } from 'react-native-easy-grid';
import { ImagePicker,DocumentPicker } from 'expo';
import * as firebase from 'firebase';
import { withNavigation } from 'react-navigation';

const firebaseConfig = {
  apiKey: "AIzaSyBMBa8FQQNlW3lzIoGK-qSHI1KNwZzmvj0",
  authDomain: "ppid-48a0a.firebaseapp.com",
  databaseURL: "https://ppid-48a0a.firebaseio.com",
  projectId: "ppid-48a0a",
  storageBucket: "ppid-48a0a.appspot.com"
}

try{
  firebase.initializeApp(firebaseConfig);
}catch(err){
  if (!/already exists/.test(err.message)) {
    console.error('Firebase initialization error', err.stack)
  }
}

function Keberatan(props){
  return (<View><CardItem header bordered style={{justifyContent:'center'}} >
                  <Text>Keberatan</Text>
                </CardItem>
                <CardItem bordered>
                  <Grid>
                    <Row>
                      <Col>
                        <Text style={{fontSize:11}}>Tanggal Pengajuan </Text>
                        <Text style={{fontSize:11}}>{ props.waktu_keberatan }</Text>
                      </Col> 
                      <Col>
                        <Text style={{fontSize:11}}>Tanggal Jawaban </Text>
                        <Text style={{fontSize:11}}>19 Maret 2019 Jam 15:30 </Text>
                      </Col>
                    </Row>
                  </Grid>
                </CardItem>
                < CardItem bordered >
                    <Text> { props.jawaban_keberatan } </Text>
                </ CardItem>
              </View>
            );
}

export default class Jawaban extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      no_pengajuan : '',
      jawaban: '',
      link_lampiran : '',
      jawaban_keberatan: '',
      waktu_keberatan: '',
      tanggal : '',
      tanggal_jawab: '',
      cara_dapat_salinan : '',
      cara_peroleh       : '',
      tujuan_pengajuan : '',
      rincian_pengajuan: '',
      status : '',
      loading: true
    };
  }

  async componentDidMount() {   
      await firebase.database().ref('Pengajuan/'+this.props.navigation.getParam('user', 'ga ada')+'/'+this.props.navigation.getParam('pengajuan', 'ga ada')).on('value',(snapshot) => {
          let jawaban_keberatan = '';
          if (snapshot.child('Keberatan/jawaban').exists()) {
            jawaban_keberatan = snapshot.val().Keberatan.jawaban;
            this.setState({waktu_keberatan:snapshot.val().Keberatan.waktu_keberatan});
          } 
          this.setState({
              no_pengajuan: snapshot.val().no_pengajuan,
              jawaban:snapshot.val().jawaban,
              link_lampiran: snapshot.val().link_lampiran,
              tanggal:snapshot.val().waktu,
              tanggal_jawab:snapshot.val().waktu_jawaban,
              cara_dapat_salinan: snapshot.val().selected2,
              cara_peroleh: snapshot.val().selected3,
              tujuan_pengajuan: snapshot.val().tujuan,
              rincian_pengajuan: snapshot.val().rincian,
              status: snapshot.val().status,
              jawaban_keberatan: jawaban_keberatan,
              loading : false
            });
      });
    
  }

  handleSelesai = ()=>{
    firebase.database().ref(`Pengajuan/${this.props.navigation.getParam('user', 'ga ada')}/${this.props.navigation.getParam('pengajuan', 'ga ada')}`).update({
      'status':'Sudah selesai'
    });
    alert('Pengajuan Anda sudah selesai');
  }


  static navigationOptions = {
    headerTitle: <LogoHeader2 />,
    headerStyle: {
      backgroundColor:'white',
      paddingBottom:20,
      height:88
    },
  };

  render() {
    if(this.state.loading){
        return (
          <Text>Sedang memuat....</Text>
        );
      }
    return (
      <Container style={{padding:5}}>
          <Content>
            <Card>
              <CardItem header bordered style={{justifyContent:'center'}} >
                <Text>{this.state.no_pengajuan}</Text>
              </CardItem>
              <CardItem bordered>
                <Grid>
                  <Row>
                    <Col>
                      <Text style={{fontSize:11}}>Tanggal Pengajuan </Text>
                      <Text style={{fontSize:11}}>{ this.state.tanggal }</Text>
                    </Col> 
                    <Col>
                      <Text style={{fontSize:11}}>Tanggal Jawaban </Text>
                      <Text style={{fontSize:11}}>{ this.state.tanggal_jawab}</Text>
                    </Col>
                  </Row>
                </Grid>
              </CardItem>
              <CardItem bordered>
                <Grid>
                  <Row>
                    <Col>
                      <Text style={{fontSize:11}}>Cara mendapatkan salinan</Text>
                      <Text style={{fontSize:11}}>{ this.state.cara_dapat_salinan }</Text>
                    </Col> 
                    <Col>
                      <Text style={{fontSize:11}}>Cara memperoleh</Text>
                      <Text style={{fontSize:11}}>{ this.state.cara_peroleh}</Text>
                    </Col>
                  </Row>
                </Grid>
              </CardItem>
              <CardItem bordered>
                  <Grid>
                  <Row>
                    <Text style={{fontSize:12,fontWeight:'bold'}}>Rincian Permohonan</Text>
                  </Row>
                  <Row>
                    <Text style={{fontSize:11}}>{this.state.rincian_pengajuan}</Text>
                  </Row>
                </Grid>
              </CardItem>
              <CardItem bordered>
                  <Grid>
                  <Row>
                    <Text style={{fontSize:12,fontWeight:'bold'}}>Tujuan Penggunaan Informasi</Text>
                  </Row>
                  <Row>
                    <Text style={{fontSize:11}}>{this.state.tujuan_pengajuan}</Text>
                  </Row>
                </Grid>
              </CardItem>
              < CardItem bordered >
                <View style={{flex: 1, flexDirection: 'column'}}>
                  <Text> {
                    (this.state.status =="Sudah dijawab")?this.state.jawaban:this.state.jawaban_keberatan
                    
                  }
                  </Text>
                  {
                      (this.state.link_lampiran != '') ? <TouchableOpacity onPress={()=>{ Linking.openURL(this.state.link_lampiran)}}><Text style={{color:"blue"}}>Klik disini untuk download dokumen pendamping</Text></TouchableOpacity>: ''
                    
                  }
                </View>              
              </ CardItem>
              {(this.state.jawaban_keberatan !='')?<Keberatan waktu_keberatan={this.state.waktu_keberatan} jawaban_keberatan={this.state.jawaban_keberatan}/>:<View></View>
              }
              <CardItem footer bordered>
                <Text> Status Pengajuan: {this.state.status}</Text>
              </CardItem>
            </Card>
          </Content>
          {
            (this.state.status == "Sudah dijawab") ?
            < View >
          <Button full success onPress={this.handleSelesai}>
            <Text>Selesai</Text>
          </Button>
          <Button danger style={{marginTop: 5}} full onPress = { () => this.props.navigation.navigate('Formkeberatan', { pengajuan: this.props.navigation.getParam('pengajuan', 'ga ada') }) } >
          <Text>Ajukan Keberatan</Text>
          </Button ></View>
          : <View></View>
          }
      </Container>
    );
  }
}