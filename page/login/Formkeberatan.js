import React, { Component } from 'react';
import { View,Image,KeyboardAvoidingView } from "react-native";
import { Container, Header, Content, Tab, Tabs, Form, Item, Input,Button,Text, H1,Textarea,Picker,Icon } from 'native-base';
import LogoHeader2 from '../../component/LogoHeader2';
import { Col, Row, Grid } from 'react-native-easy-grid';
import * as FileSystem from 'expo-file-system';
//import { ImagePicker,DocumentPicker } from 'expo';
import * as ImagePicker from 'expo-image-picker';
import * as DocumentPicker from 'expo-document-picker';
import * as firebase from 'firebase';
import { withNavigation } from 'react-navigation';
import Moment from 'moment';

const firebaseConfig = {
  apiKey: "AIzaSyBMBa8FQQNlW3lzIoGK-qSHI1KNwZzmvj0",
  authDomain: "ppid-48a0a.firebaseapp.com",
  databaseURL: "https://ppid-48a0a.firebaseio.com",
  projectId: "ppid-48a0a",
  storageBucket: "ppid-48a0a.appspot.com"
}

try{
  firebase.initializeApp(firebaseConfig);
}catch(err){
  if (!/already exists/.test(err.message)) {
    console.error('Firebase initialization error', err.stack)
  }
}


export default class Formkeberatan extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      alasan: '',
      informasi : null,
      rincian : null,
      user : '',
    };
  }

  componentDidMount() {
    firebase.auth().onAuthStateChanged(user => {
        this.setState({user : user});
    })
  }

   onValueChangeAlasan(value: string) {
    this.setState({
      alasan: value
    });
  }

  static navigationOptions = {
    headerTitle: <LogoHeader2 />,
    headerStyle: {
      backgroundColor:'white',
      paddingBottom:20,
      height:88
    },
  };

  kirimKeberatan = () => {
      let informasi = this.state.informasi;
      let alasan = this.state.alasan;
      let rincian = this.state.rincian;
      let indonesia = require('moment/locale/id');
      Moment.updateLocale('id', indonesia);
      let waktu_keberatan = Moment().format('LLL');
      firebase.database().ref('Pengajuan/'+ this.state.user.uid+'/'+this.props.navigation.getParam('pengajuan', 'ga ada')+'/Keberatan').set({
            alasan,
            informasi,
            rincian,
            waktu_keberatan
        }).then((data)=>{
            firebase.database().ref('Pengajuan/' + this.state.user.uid + '/' + this.props.navigation.getParam('pengajuan', 'ga ada')).update({'status' : 'Keberatan sedang di review'});
            this.props.navigation.navigate('Dashboarduser');
        }).catch((error)=>{
            this.setState({ errorMessage: error.message })
        });

  }

  render() {
    return (
      <Container style={{padding:10}}>
        <KeyboardAvoidingView behavior="padding" style={{ flex: 1 }}>
          <Content padder>
            <Form>
              <Textarea onChangeText={informasi=>this.setState({informasi})} rowSpan={5} bordered placeholder="Informasi Pengajuan Keberatan" />

              <Textarea onChangeText={rincian=>this.setState({rincian})} rowSpan={5} bordered placeholder="Rincian Pengajuan Keberatan" />

              <View style={{marginVertical:10}}>
                <Text>Alasan Pengajuan Keberatan</Text>
                <Item picker>
                  <Picker
                    mode="dropdown"
                    placeholder="Cara memperoleh Informasi"
                    placeholderStyle={{ color: "#bfc6ea" }}
                    placeholderIconColor="#007aff"
                    selectedValue={this.state.alasan}
                    onValueChange={this.onValueChangeAlasan.bind(this)}
                  >
                    <Picker.Item label="Permohonan Informasi ditolak" value="Permohonan Informasi ditolak" />
                    <Picker.Item label="Informasi berkala tidak disediakan" value="Informasi berkala tidak disediakan" />
                    <Picker.Item label="Permintaan informasi tidak ditanggapi" value="Permintaan informasi tidak ditanggapi" />
                    <Picker.Item label="Permintaan informasi ditanggapi tidak sebagaiman yang diminta" value="Permintaan informasi ditanggapi tidak sebagaiman yang diminta" />
                    <Picker.Item label="Biaya yang dikenakan tidak wajar" value="Biaya yang dikenakan tidak wajar" />
                    <Picker.Item label="Informasi disampaikan melebihi jangka waktu yang ditentukan" value="Informasi disampaikan melebihi jangka waktu yang ditentukan" />
                  </Picker>
                </Item>
              </View>

              <Button style={{marginTop:20}} full onPress={this.kirimKeberatan}>
                <Text>Kirim Keberatan</Text>
              </Button>
            </Form>
          </Content>
        </KeyboardAvoidingView>
      </Container>
    );
  }
}