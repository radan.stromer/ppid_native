import React, { Component } from 'react';
import { View,Image,KeyboardAvoidingView,StyleSheet,SafeAreaView } from "react-native";
import { WebView } from 'react-native-webview';
import { Container, Header, Content, Tab, Tabs, Form, Item, Input,Button,Text, H1, Textarea } from 'native-base';
import LogoHeader2 from '../../component/LogoHeader2';
import { Col, Row, Grid } from 'react-native-easy-grid';
import * as firebase from 'firebase';
import GoogleReCaptcha from 'rn-google-recaptcha-v2';

const siteKey = '6LfZFdkUAAAAALib79ReeaQafUOl3u1ONFH-YO9N';
const baseUrl = 'http://dadan.id';

const firebaseConfig = {
  apiKey: "AIzaSyBMBa8FQQNlW3lzIoGK-qSHI1KNwZzmvj0",
  authDomain: "ppid-48a0a.firebaseapp.com",
  databaseURL: "https://ppid-48a0a.firebaseio.com",
  projectId: "ppid-48a0a",
  storageBucket: "ppid-48a0a.appspot.com"
}

try{
  firebase.initializeApp(firebaseConfig);
}catch(err){
  if (!/already exists/.test(err.message)) {
    console.error('Firebase initialization error', err.stack)
  }
}




export default class Signup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email : '',
      pass : '',
      ktp  : '',
      nama:'',
      nik:'',
      telp:'',
      alamat:'',
      pekerjaan : '',
      errorMessage: null,
      recaptchaViewHeight: 90, //initial default height
      showSubmit : false
    }
  }

  static navigationOptions = {
    headerTitle: <LogoHeader2 />,
    headerStyle: {
      backgroundColor:'white',
      paddingBottom:20,
      height:88
    },
  };

  handleSignUp = () => {
    if(this.state.email<6){
      alert('Password minimal 6 karakter');
      return
    }
    firebase.auth().createUserWithEmailAndPassword(this.state.email,this.state.pass)
      .then( (res) =>{
        firebase
          .auth()
          .signInWithEmailAndPassword(this.state.email, this.state.pass)
          .then(() => {
            console.log('tela login');
            this.writeUserData(res.user.uid, this.state.email, this.state.nama, this.state.ktp, this.state.telp, this.state.pekerjaan, this.state.nik, this.state.alamat)
          })
          .catch(error => this.setState({
            errorMessage: error.message
          }))
    
        
      }
      )
      .catch(error => this.setState({ errorMessage: error.message }));

      
  
  }

  writeUserData(uid, email,nama,ktp,telp,pekerjaan,nik,alamat){
      var user = firebase.auth().currentUser;
      user.sendEmailVerification()
        .then(function () {
          console.log('kirm');
        })
        .catch(function (error) {
          console.log('tak kirim verifikasi');
        });

        firebase.database().ref('Users/' + uid).set({
          email,
          telp,
          nama,
          alamat,
          pekerjaan,
          nik,
          ktp
        }).then((data) => {
          this.props.navigation.navigate('Login')
        }).catch((error) => {
          //error callback
          this.setState({
            errorMessage: error.message
          })
        });
  }

  render() {
    const { recaptchaViewHeight } = this.state;
    return (
            <Content padder>
              <Text style={{fontWeight:'bold', fontSize:14,marginBottom:10, textAlign:'justify'}}>Silahkan mendaftar untuk mengajukan permohonan informasi maupun pengajuan keberatan.</Text>
              {this.state.errorMessage &&
              <Text style={{ color: 'red' }}>
                {this.state.errorMessage}
              </Text>}
                <KeyboardAvoidingView behavior="padding" style={{ flex: 1,}} enabled>
                <Form>
                  <Item regular style={{marginBottom:15}}>
                    <Input value={this.state.nik} placeholder="No KTP" onChangeText={nik=> this.setState({nik})} keyboardType="number-pad"/>
                  </Item>

                  <Item regular style={{marginBottom:15}}>
                    <Input value={this.state.nama} placeholder="Nama" onChangeText={nama=> this.setState({nama})}/>
                  </Item>

                  <Item regular style={{marginBottom:15}}>
                    <Input value={this.state.pekerjaan} placeholder="Pekerjaan" onChangeText={pekerjaan=> this.setState({pekerjaan})}/>
                  </Item>

                  <Textarea style={{marginBottom:15}} value={this.state.alamat} rowSpan={5} bordered placeholder="Alamat" onChangeText={alamat=> this.setState({alamat})}/>
                  
                  <Item regular style={{marginBottom:15}}>
                    <Input value={this.state.telp} placeholder="No Telp" onChangeText={telp=> this.setState({telp})} keyboardType="phone-pad"/>
                  </Item>

                  <Item regular style={{marginBottom:15}}>
                    <Input value={this.state.email} placeholder="Email" onChangeText={email=> this.setState({email})} keyboardType="email-address"/> 
                  </Item>
                  <Item regular style={{marginBottom:15}}>
                    <Input value={this.state.pass} placeholder="Password" secureTextEntry onChangeText={pass=> this.setState({pass})}/>
                  </Item>
                  <SafeAreaView style={{flex:1}}>
                      <GoogleReCaptcha
                          style={{ height: recaptchaViewHeight }}
                          siteKey={siteKey}
                          url={baseUrl}
                          languageCode="en"
                          onMessage={this.onRecaptchaEvent} />
                  </SafeAreaView>
                 
                  {this.state.showSubmit &&
                    <Button onPress={this.handleSignUp}>
                      <Text>
                        Daftar
                      </Text>
                    </Button>
                  }
                </Form>
            </KeyboardAvoidingView>

            </Content>
    );
  }


  onRecaptchaEvent = event => {
    if (event && event.nativeEvent.data) {
        const data = decodeURIComponent(
            decodeURIComponent(event.nativeEvent.data),
        );
        if (data.startsWith('CONTENT_PARAMS:')) {
            let params = JSON.parse(data.substring(15));
            let recaptchaViewHeight = params.visibility === 'visible' ? params.height : 90;
            this.setState({ recaptchaViewHeight });
        } else if (['cancel', 'error', 'expired'].includes(data)) {
            return;
        } else {
            console.log('Verified code from Google', data);
            this.setState({ recaptchaToken: data });
            this.setState({ showSubmit: true });
        }
    }
  };
}