//basic react native
import React from 'react';
import { StyleSheet, View,Image,ImageBackground } from "react-native";

//native base
import { Container, Header, Content, Button, Text } from 'native-base';
import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';

//react navigation
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import Landing from "./page/Landing";
import Dashboard from "./page/Dashboard";
import MainProfil from "./page/profil/MainProfil";
import MainPublik from "./page/publik/MainPublik";

import Tab1pub from "./page/publik/Tab1";
import Tab4pub from "./page/publik/Tab4";

import MainLayanan from "./page/layanan/MainLayanan";
import Tab1 from "./page/layanan/Tab1";
import Tab2 from "./page/layanan/Tab2";
import Tab3 from "./page/layanan/Tab3";
import Tab4 from "./page/layanan/Tab4";
import Tab5 from "./page/layanan/Tab5";
import Tab6 from "./page/layanan/Tab6";
import Tab7 from "./page/layanan/Tab7";
import Faq from "./page/Faq";

import Regulasi from "./page/Regulasi";
import Regulasi1 from "./page/regulasi/Regulasi1";
import Regulasi2 from "./page/regulasi/Regulasi2";
import Regulasi3 from "./page/regulasi/Regulasi3";

import Kecuali from "./page/publik/Kecuali";

import Login from "./page/login/Login";
import Signup from "./page/login/Signup";
import Lupa from "./page/login/Lupa";

import Dashboarduser from "./page/login/Dashboarduser";
import Formpengajuan from "./page/login/Formpengajuan";
import Formkeberatan from "./page/login/Formkeberatan";
import Jawaban from "./page/login/Jawaban";
import Struktur from "./page/profil/Struktur";
import Tab1prof from "./page/profil/Tab1";
import Jobsinfo from "./page/publik/setiapsaat/Jobsinfo";
import UpgradeSkill from "./page/publik/setiapsaat/UpgradeSkill";

import Ggjapan from "./page/publik/setiapsaat/Ggjapan";
import Ggkorea from "./page/publik/setiapsaat/Ggkorea";
import Kur from "./page/publik/setiapsaat/Kur";
import Penempatanmi from "./page/publik/setiapsaat/Penempatanmi";
import Terpadu from "./page/publik/setiapsaat/Terpadu";

import Tppo from "./page/publik/setiapsaat/Tppo";
import Crisiscenter from "./page/publik/setiapsaat/Crisiscenter";
import MediaAvokasi from "./page/publik/setiapsaat/MediaAvokasi";
import Pkpmi from "./page/publik/setiapsaat/Pkpmi";
import Pemberdayaan from "./page/publik/setiapsaat/Pemberdayaan";
import Pencegahan from "./page/publik/setiapsaat/Pencegahan";
import Penguatan from "./page/publik/setiapsaat/Penguatan";

import Berkala1 from "./page/publik/berkala/Berkala1";
import Berkala2 from "./page/publik/berkala/Berkala2";
import Berkala3 from "./page/publik/berkala/Berkala3";

import LogoHeader2 from './component/LogoHeader2';



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const Tab = createBottomTabNavigator(
  {
    Profil: MainProfil,
    Publik : MainPublik,
    Layanan : MainLayanan,
    Regulasi : Regulasi,
    Faq   :  Faq,
    Login   :  Login
  },
  {
    defaultNavigationOptions: ({
      navigation
    }) => ({
      tabBarIcon: ({
        focused,
        horizontal,
        tintColor
      }) => {
        const { routeName } = navigation.state;
        let url;
        if (routeName === 'Layanan') {
          url = require('./assets/iconppid/iconsubmenu/standarlayanan.png');
        } else if (routeName === 'Profil') {
          url = require('./assets/iconppid/iconsubmenu/profil.png');
        }else if (routeName === 'Regulasi') {
          url = require('./assets/iconppid/iconsubmenu/regulasi.png');
        }else if (routeName === 'Faq') {
          url = require('./assets/iconppid/iconsubmenu/qna.png');
        }else if (routeName === 'Publik') {
          url = require('./assets/iconppid/iconsubmenu/informasi.png');
        }else{
          url = require('./assets/iconppid/iconsubmenu/layanan.jpg');
        }
        // You can return any component that you like here!
        return <Image source = { url } style = { { width: 20, height: 20 } } />;
      }
    })
  }
);

Tab.navigationOptions = ({ navigation }) => {

  // You can do whatever you like here to pick the title based on the route name
  const headerTitle = <LogoHeader2 />;
  const headerStyle = {
      backgroundColor:'white',
      paddingBottom:10,
      height:88
    };

    
  return {
    headerTitle,headerStyle
  };
};

const AppNavigator = createStackNavigator({
  // Tabs: Tab,
   Home: Landing,
   Dashboard: Dashboard,
   Profil: Tab,
   Layanan1: Tab1,
   Layanan2: Tab2,
   Layanan3: Tab3,
   Layanan4: Tab4,
   Layanan5: Tab5,
   Layanan6: Tab6,
   Layanan7: Tab7,
   Regulasi1: Regulasi1,
   Regulasi2: Regulasi2,
   Regulasi3: Regulasi3,
   Tab1pub : Tab1pub,
   Tab4pub : Tab4pub,
   Kecuali: Kecuali,
   Signup: Signup,
   Lupa: Lupa,
   Dashboarduser: Dashboarduser,
   Formpengajuan: Formpengajuan,
   Formkeberatan: Formkeberatan,
   Jawaban: Jawaban,
   Struktur: Struktur,
   Tab1prof : Tab1prof,
   Jobsinfo : Jobsinfo,
   UpgradeSkill:UpgradeSkill,
   Ggkorea : Ggkorea,
   Ggjapan : Ggjapan,
   Kur : Kur,
   Penempatanmi : Penempatanmi,
   Terpadu : Terpadu,
   Tppo : Tppo,
   Crisiscenter : Crisiscenter,
   MediaAvokasi : MediaAvokasi,
   Pkpmi : Pkpmi,
   Pemberdayaan : Pemberdayaan,
   Pencegahan : Pencegahan,
   Penguatan : Penguatan,
   Berkala1 : Berkala1,
   Berkala2 : Berkala2,
   Berkala3 : Berkala3
 },
 {
   initialRouteName: "Dashboard"
 },
);

const AppContainer = createAppContainer(AppNavigator);

export default class App extends React.Component {
  render() {
    return (
      <AppContainer/>
    );
  }
}