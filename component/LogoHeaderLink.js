import React from "react";
import { View,Image,Linking } from "react-native";
import {Button} from 'native-base';

export default class LogoTitle extends React.Component {
  render() {
    return (
        <View style={{flex:1,flexDirection:'row', justifyContent:'flex-end'}}>
          <Button onPress={ ()=>{ Linking.openURL('http://www.bnp2tki.go.id/')}}>
            <Image
              source={require('../assets/kopaplikasiPPID-1.png')}
              style={{ width:325,height: 57}}
            />
          </Button>
        </View>
    );
  }
}