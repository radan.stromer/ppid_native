import React from "react";
import { StyleSheet, View,Image,ImageBackground,Dimensions,TouchtableOpacity } from "react-native";
import { Container, Header, Content, Button, Text, Card, CardItem, Icon, H1,Footer } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';

export default class LogoTitle extends React.Component {
  render() {
    return (
      <Grid>
      <Col size={1}>
        <Image
          source={require('../assets/splash.png')}
          style={{ width: 70, height: 85 }}
        />
      </Col>
      <Col size={3}>
        <Text style={{fontSize:30,color:'white'}}>PPID</Text>
        <Text style={{fontSize:30,color:'white'}}>BNP2TKI</Text>
      </Col>
      </Grid>
    );
  }
}